## Integration

Developers use this project to plan, learn, share, and design for the integration of ViewOrder.

This project will center on the integration testing of various components of ViewOrder in order to evaluate the compliance of possibly multiple components with ViewOrder's functional requirements.

This project will be home to test cases, test scenarios, test scripts and tests results, along with any possible defects/issues. The results of all of these will also be documented in this project.

---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
